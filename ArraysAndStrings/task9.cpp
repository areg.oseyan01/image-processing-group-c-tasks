#include <iostream>
#include <cmath>
#include <string>
#include <cstring>

bool foo(std::string pat, std::string txt, int pos)
{
  for (int i = 0; i < pat.size(); ++i)
  {
    if (pat[i] != txt[pos]) { return false; }
    pos++;
  }
  return true;
}

// Rabin-Karp Algorithm
bool search(std::string pat, std::string txt)
{
  int m = pat.size();
  int n = txt.size();
  int pos = 0;
  int hashPat = 0;
  int hashTxt = 0;

  for (int i = 0; i < m; ++i)
  {
    hashPat = (hashPat * 10 + int(pat[i])) % (m + n);
  }

  pos = 0;
  for (int i = 0; i < m; ++i)
  {
    hashTxt = (hashTxt * 10 + int(txt[i])) % (m + n);
  }
  int q = 1;
  for (int i = 0; i < m - 1; ++i)
  {
    q = (q * 10) % (n + m);
  }
  if (hashTxt == hashPat)
  {
    if (foo(pat, txt, pos)) { return true; }
  }
  
  for (int j = 0; j < n; ++j)
  {
    pos = j + 1;
    hashTxt = ((hashTxt - (int(txt[j]) * q)) * 10 + int(txt[j + m])) % (n + m);
    if (hashTxt < 0) { hashTxt += (n + m); }
    if (hashTxt == hashPat)
    {
      if (foo(pat, txt, pos)) { return true; }
    }
  }
  return false;
}

bool isRotation(std::string s1, std::string s2) 
{
  if (s1.size() == s2.size()) 
  {
    std::string ss;
    for (int i = 0; i < s1.size(); ++i)
    {
      ss += s1[i];
      std::string s11 = s1.substr(i + 1) + ss;
      if (search(s11, s2)) { return true; }
    }
  }
  return false;
}

int main()
{
  std::string txt = "waterbottle";
  std::string pat = "erbottlewat";
  std::string pat1 = "prbottlewat";

  if (isRotation(pat1, txt))
  { std::cout << "Is rotation" << std::endl; }
  else { std::cout << "Isn't rotation\n"; }
  
  return {};
}
