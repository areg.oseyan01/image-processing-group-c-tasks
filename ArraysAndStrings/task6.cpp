#include <iostream>
#include <string>
#include <unordered_map>

std::string stringCompression(const std::string& str)
{
  std::string s = "";
  s += str[0];
  int count = 1;
  char help = str[0];
  for (int i = 1; i < str.size(); ++i)
  {
    if (help == str[i]) { count++; }
    else
    {
      s += count + '0';
      s += str[i]; 
      count = 1;
      help = str[i];
    }
  }
  s += count + '0';
  return s.size() > str.size() ? str : s;
}

int main()
{
  std::string s1 = "aabcccccaaa";
  std::string s2 = "a";
  std::cout << s1 << " Result is " << stringCompression(s1) << std::endl;
  std::cout << s2 << " Result is " << stringCompression(s2) << std::endl;

  return {};
}
