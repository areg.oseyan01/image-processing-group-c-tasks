#include <iostream>
#include <vector>
#include <unordered_map>

void setZeroRow(int** matrix, int col, int i)
{
  for (int j = 0; j < col; ++j) { matrix[i][j] = 0; }
}

void setZeroCol(int** matrix, int row, int j)
{
  for (int i = 0; i < row; ++i) { matrix[i][j] = 0; }
}

void setZero(int** matrix, int row, int col)
{
  std::vector<bool> rows(row, false);
  std::vector<bool> colms(col, false);
  for (int i = 0; i < row; ++i)
  {
    for (int j = 0; j < col; ++j)
    {
      if (matrix[i][j] == 0) 
      { 
        rows[i] = true;
        colms[j] = true;
      }
    }
  }

  for (int i = 0; i < rows.size(); ++i)
  {
    if (rows[i]) { setZeroRow(matrix, col, i); }
  }
  for (int j = 0; j < colms.size(); ++j)
  {
    if (colms[j]) { setZeroCol(matrix, row, j); }
  }
}

void printMatrix(int** matrix, int row, int col)
{
  for (int i = 0; i < row; ++i)
  {
    for (int j = 0; j < col; ++j)
    {
      std::cout << matrix[i][j] << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

int main()
{
  int row, colm;
  std::cout << "Please enter matrix size(NxN): ";
  std::cin >> row;
  colm = row;
  int** arr = new int*[row];                

  for (int i = 0; i < row; ++i)
  {
    arr[i] = new int[colm];
  }
     
  for (int i = 0; i < row; ++i)
  {
    for (int j = 0; j < colm; ++j)
    {
      std::cin >> arr[i][j];
    }
  }
  std::cout << "Result is \n";
  setZero(arr, row, colm);
  printMatrix(arr, row, colm);

  return {};
}
