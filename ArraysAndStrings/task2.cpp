#include <iostream>
#include <unordered_map>

bool isPermutation(const std::string& s1, const std::string& s2)
{
  std::unordered_map<char, int> helper;
  for (auto x : s1) { helper[x]++; }
  for (auto y : s2) { helper[y]--; }
  for (auto z : helper) { if (z.second) { return false; } }
  return true;
}

int main()
{
  std::string st1 = "acb";
  std::string st2 = "bca";
  if (isPermutation(st1, st2)) 
  { 
    std::cout << "Is a permutation" << std::endl;
  }
  else 
  {
    std::cout << "Isnt a permutation" << std::endl;
  }

  return {};
}
