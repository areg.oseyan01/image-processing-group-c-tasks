#include <algorithm>
#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>

template <typename iter, typename outputIter, typename predicate>
outputIter copy_if (iter begin, iter end, outputIter start, predicate pr) 
{
  for ( ; begin != end; ++begin) 
  {
    if (pr(*begin)) 
    {
      *start = *begin;
      start++;
    }
  }
  return start;
}

int main()
{
  std::vector<int> vec1 = { 120, 140, 200, 30, 5, 6, 12 };
  std::vector<int> vec2;

  ::copy_if(vec1.begin(), vec1.end(), std::back_inserter(vec2), [] (int x) { return x % 10 == 0; } );
  for (int i = 0; i < vec2.size(); ++i)
  {
    std::cout << vec2[i] << " ";
  }
  std::cout << std::endl;

  return 0;
}
