#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <typeinfo>
#include <ctime>
#include <algorithm>

template <typename T>
void insertionSort(std::vector<T>& arr)
{
  T key;
  int j = 0;
  for (int i = 1; i < arr.size(); ++i)
  {
    key = arr[i];
    j = i - 1;
    while (j >= 0 && arr[j] > key)
    {
      arr[j + 1] = arr[j];
      j = j - 1;
    }
    arr[j + 1] = key;
  }
}

template <typename T>
void marge(std::vector<T>& arr, std::vector<T> const& left, std::vector<T> const& right)
{
  size_t i = 0;
  size_t j = 0;
  while (i < left.size() || j < right.size())
  {
    if (i == left.size())
    {
      arr[i + j] = right[j];
      j++;
    }
    else if (j == right.size())
    {
      arr[i + j] = left[i];
      i++;
    }
    else if (left[i] <= right[j])
    {
      arr[i + j] = left[i];
      i++;
    }
    else
    {
      arr[i + j] = right[j];
      j++;
    }
  }
}

template <typename T>
void margeSorting(std::vector<T>& arr)
{
  if (arr.size() < 2) { return; }

  size_t n = arr.size() / 2;
  size_t m = arr.size() - n;
  std::vector<T> left;
  left.reserve(n);
  std::vector<T> right;
  right.reserve(m);
  for (size_t i = 0; i < n; ++i)
    left.push_back(arr[i]);
  for (size_t j = n; j < arr.size(); ++j)
    right.push_back(arr[j]);

  margeSorting(left);
  margeSorting(right);
  marge(arr, left, right);
}

template <typename T>
int partition(std::vector<T>& arr, int beginning, int end) 
{
  int index = std::floor((end - beginning) / 2);
  int med = std::max(std::min(arr[beginning], arr[index]), std::min(std::max(arr[beginning], arr[index]), arr[end]));
  if (med == arr[index]) { std::swap(arr[beginning], arr[index]); }
  else if (med == arr[end]) { std::swap(arr[beginning], arr[end]); }
  T pivot = arr[beginning];

  int i = beginning + 1;
  for (int j = beginning + 1; j <= end; ++j) 
  {
    if (arr[j] < pivot) 
    { 
      std::swap(arr[j], arr[i]);
      i++;
    } 
  } 
  std::swap(arr[beginning], arr[i - 1]); 
  return i - 1; 
}

template <typename T>
void quickSort(std::vector<T>& arr, int beginning, int end) 
{
  if (beginning < end) 
  { 
    int pivot = partition(arr, beginning, end); 

    quickSort(arr, beginning, pivot - 1); 
    quickSort(arr, pivot + 1, end);
  }
}

template <typename T>
void sorting(std::vector<T>& arr)
{
  std::string type = typeid(T).name();
  if (type == "d") 
  {
    std::cout << "Insertion sort\n";
    insertionSort<T>(arr);
    return;
  }
  if (arr.size() < 5000) 
  { 
    std::cout << "Marge sort\n";
    margeSorting<T>(arr); 
    return; 
  }
  std::cout << "Quick sort\n";
  quickSort<T>(arr, 0, arr.size() - 1);
}

template <typename T>
void printArr(std::vector<T>& array)
{
  for (size_t i = 0; i < array.size(); ++i)
  {
    std::cout << array[i] << " ";
  }
  std::cout << std::endl;
}

int main()
{
  std::vector<double> arr1 = { 5., 4., 3., 2., 0. };
  sorting<double>(arr1);

  const int limite2 = 4999;
  srand(time(NULL)); 
  std::vector<int> arr2;
  arr2.reserve(limite2);

  for (int i = 0; i < limite2; ++i) { arr2.push_back(rand()); }
  sorting<int>(arr2);

  const int limite3 = 5001;
  srand(time(NULL));
  std::vector<int> arr3;
  arr3.reserve(limite3);

  for (int i = 0; i < limite3; ++i) { arr3.push_back(rand()); }
  sorting<int>(arr3);

  printArr(arr1);
  std::cout << "New array start\n\n";
  printArr(arr2);
  std::cout << "New array start\n\n";
  std::cout << std::endl;
  printArr(arr3);
  std::cout << std::endl;

  if (std::is_sorted(arr1.begin(), arr1.end())) { std::cout << "Array is sorted\n"; }
  if (std::is_sorted(arr2.begin(), arr2.end())) { std::cout << "Array is sorted\n"; }
  if (std::is_sorted(arr3.begin(), arr3.end())) { std::cout << "Array is sorted\n"; }
  return 0;
}
