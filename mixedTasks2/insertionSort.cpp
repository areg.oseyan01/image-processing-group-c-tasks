#include <iostream>

void insertionSort(int* arr, int size)
{
  int key;
  int j = 0;
  for (int i = 1; i < size; ++i)
  {
    key = arr[i];
    j = i - 1;
    while (j >= 0 && arr[j] > key)
    {
      arr[j + 1] = arr[j];
      j = j - 1;
    }
    arr[j + 1] = key;
  }
}

int main()
{
  int array[] = { 5, 4, 3, 2, 0 };
  insertionSort(array, 5);
  for (int i = 0; i < 5; ++i)
  {
    std::cout << array[i] << " ";
  }
  std::cout << std::endl;

  return 0;
}
