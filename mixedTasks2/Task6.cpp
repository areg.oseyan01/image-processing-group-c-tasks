#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <time.h>

bool generator(std::string fname) 
{
  std::ofstream file;
  file.open(fname);
  if (!file)
  {
    std::cout << "File not find!!!\n";
    file.close();
    return false;
  }
	const size_t maxNumberCount = 4000000000;
	size_t countNumber = 0;
  unsigned char num;

	srand(time(NULL));
	while (countNumber < maxNumberCount) 
  {
		num = (rand() % 200 + 1); // number = [1, 200]
		file << num;
		countNumber += 1;
	}
	std::cout << "Number count is " << countNumber << std::endl;
  file.close();
	return true;
}

void ReadsFromFiles(std::string fname)
{
  std::ifstream MyFile(fname);
  unsigned char num; 
  while (MyFile >> num) 
  { 
    int b = static_cast<int>(num);
    std::cout << b << " "; 
  }

  MyFile.close();
}


int main()
{
  generator("text.txt"); 
  ReadsFromFiles("text.txt");

  return 0;
}
