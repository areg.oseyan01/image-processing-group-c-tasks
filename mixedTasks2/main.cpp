#include <iostream>
#include "avl.hpp"
#include <limits>

int main()
{
  avl<int> avlTree;
  avlTree.insert(14);
  avlTree.insert(3);
  avlTree.insert(12);
  avlTree.insert(8);
  //avlTree.insert(9);
  avlTree.insert(25);
  //avlTree.insert(6);
  //avlTree.insert(20);
  avlTree.insert(30);
  avlTree.insert(40);
  avlTree.insert(4);
  avlTree.insert(2);


  std::cout << "In Order print\n";
  avlTree.printInOrder();
  std::cout << std::endl;
  /*avlTree.printInOrderWithStack();
  std::cout << std::endl;*/

  //avlTree.del(2);
  //avlTree.del(12);

  avlTree.del(30);
  avlTree.del(14);
  std::cout << "After delete \n";
  avlTree.printInOrder();
  std::cout << std::endl;


  /*std::cout << "Pre Order print\n";
  avlTree.printPreOrder();
  std::cout << std::endl;

  std::cout << "m_size is " << avlTree.getSize() << std::endl;
  std::cout << "size is " << avlTree.size() << std::endl;
  std::cout << "Height is " << avlTree.getHeight() << std::endl;
  std::cout << "Height is(with stack realization) " << avlTree.getHeightWithStack() << std::endl;

  std::cout << "Max elem is " << avlTree.max() << std::endl;
  std::cout << "Min elem is " << avlTree.min() << std::endl;
  std::cout << "Maximum width is " << avlTree.width() << std::endl;
  std::cout << "Successor of 8 is " << avlTree.successor(8) << std::endl;
  std::cout << "Predecessor of 30 is " << avlTree.predecessor(30) << std::endl;

  avlTree.findKey(5);
  avlTree.findKey(6);

  if (avlTree.isBST()) { std::cout << "Is BST tree\n"; }
  else { std::cout << "BST is not a tree!!!\n"; }

  avlTree.clear();*/


  return 0;
}
