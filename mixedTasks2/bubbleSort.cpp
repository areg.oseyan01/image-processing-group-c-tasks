#include <iostream>

void bubbleSort(int* array, int size)
{
  bool swapped;
  for (int i = 0; i < size - 1; ++i)
  {
    swapped = false;
    for (int j = 0; j < size - i - 1; ++j)
    {
      if (array[j] > array[j + 1])
      {
        std::swap(array[j], array[j + 1]);
        swapped = true;
      }
    }
    if (!swapped) { break; }
  }
}

int main()
{
  int array[] = { 5, 4, 3, 2, 0 };
  bubbleSort(array, 5);
  for (int i = 0; i < 5; ++i )
  {
    std::cout << array[i] << " ";
  }
  std::cout << std::endl;

  return 0;
}
