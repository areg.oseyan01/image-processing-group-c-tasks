#include <iostream>

int fibo(int n, int x1 = 1, int x2 = 1)
{
  if (n == 0) { return x1; }
  if (n == 1) { return x2; }
  return fibo(n - 1, x2, x1 + x2);
}

int main()
{
  int n;
  std::cout << "Please enter number\n";
  std::cin >> n;
  std::cout << "fibo[" << n << "] = " << fibo(n) << std::endl;

  return 0;
}
