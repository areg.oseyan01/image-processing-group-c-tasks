#include <iostream>

void selectionSort(int* arr, int size)
{
  int j;
  for (int i = 0; i < size - 1; ++i)
  {
    j = i;
    for (int k = i + 1; k < size; ++k)
    {
      if (arr[k] < arr[j]) { j = k; }
    }
    if (arr[i] > arr[j])
    {
      int tmp = arr[i];
      arr[i] = arr[j];
      arr[j] = tmp;
    }
  }
}

int main()
{
  int array[] = { 5, 4, 3, 2, 0 };
  selectionSort(array, 5);
  for (int i = 0; i < 5; ++i)
  {
    std::cout << array[i] << " ";
  }
  std::cout << std::endl;

  return 0;
}
