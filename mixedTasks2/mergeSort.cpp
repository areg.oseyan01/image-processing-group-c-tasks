#include <iostream>
#include <vector>

void marge(std::vector<int>& arr, std::vector<int> const& left, std::vector<int> const& right)
{
  size_t i = 0;
  size_t j = 0;
  while (i < left.size() || j < right.size()) 
  {
    if (i == left.size()) 
    {
      arr[i + j] = right[j];
      j++;
    } 
    else if (j == right.size()) 
    {
      arr[i + j] = left[i];
      i++;
    } 
    else if (left[i] <= right[j]) 
    {
      arr[i + j] = left[i];
      i++;
    } 
    else 
    {
      arr[i + j] = right[j];
      j++;
    }
  }
}

void sorting(std::vector<int>& arr)
{
  if (arr.size() < 2) { return; }

  size_t n = arr.size() / 2;
  size_t m = arr.size() - n;
  std::vector<int> left;
  left.reserve(n);
  std::vector<int> right;
  right.reserve(m);
  for (size_t i = 0; i < n; ++i)
    left.push_back(arr[i]);
  for (size_t j = n; j < arr.size(); ++j)
    right.push_back(arr[j]);

  sorting(left);
  sorting(right);
  marge(arr, left, right);
}

int main()
{
  std::vector<int> array = { 5, 4, 3, 2, 0 };
  sorting(array);
  for (size_t i = 0; i < array.size(); ++i)
  {
    std::cout << array[i] << " ";
  }
  std::cout << std::endl;

  return 0;
}
